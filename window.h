#pragma once

#include "link.h"

class window
{
private:
	    static window* wndInstance;
        HDC                     hDC;            // Private GDI Device Context
        HGLRC           hRC;            // Permanent Rendering Context
        HWND            hWnd;           // Holds Our Window Handle
        HINSTANCE       hInstance;              // Holds The Instance Of The Application
        bool    keys[256];
        bool active;
        int x;
        int y;

        bool WindowGL(char* title, int width, int height, int bits);
        GLvoid KillWindow(GLvoid);
        int FrontEnd(GLvoid);
        int Init(GLvoid);
        static bool CALLBACK StaticProc(HWND    hWnd, UINT      uMsg, WPARAM    wParam, LPARAM  lParam);
        bool WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
        GLvoid WindowResize(int width, int height);
		

public:
	    window()
		{
		
		}
		static window* getInstance(); // creating singletone instance to access static function from non-static function
        int Window(char* Title);
};