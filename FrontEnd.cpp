#include "window.h"
#include "link.h"

int window::FrontEnd(GLvoid)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();		

	glTranslatef(-0.0f,0.0f,-1.0f);
	glBegin(GL_QUADS);
		glColor3f(   0.00f,	0.00f, 1.0f);
		glVertex3f( -0.63f, 0.45f, 0.0f);
		glVertex3f(  0.63f, 0.45f, 0.0f);
		glVertex3f(  0.63f, 0.35f, 0.0f);
		glVertex3f( -0.63f, 0.35f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f (	 1.00f,	0.00f, 0.0f);
		glVertex3f(  0.54f, 0.45f, 0.0f);
		glVertex3f(  0.63f, 0.45f, 0.0f);
		glVertex3f(  0.63f, 0.35f, 0.0f);
		glVertex3f(  0.54f, 0.35f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f(	 0.00f,	1.00f, 0.0f);
		glVertex3f(  0.45f, 0.45f, 0.0f);
		glVertex3f(  0.54f, 0.45f, 0.0f);
		glVertex3f(  0.54f, 0.35f, 0.0f);
		glVertex3f(  0.45f, 0.35f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f (	 0.00f,	1.00f, 1.0f);
		glVertex3f(  0.36f, 0.45f, 0.0f);
		glVertex3f(  0.45f, 0.45f, 0.0f);
		glVertex3f(  0.45f, 0.35f, 0.0f);
		glVertex3f(  0.36f, 0.35f, 0.0f);
	glEnd();

	return true;
}