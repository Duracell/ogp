#include "window.h"
#include "link.h"

bool window::WindowGL(char* title, int width, int height, int bits)
{
	GLuint		PixelFormat;
	WNDCLASS	wc;
	DWORD		dwExStyle;
	DWORD		dwStyle;
	RECT		WindowRect;
	WindowRect.left		= 0;
	WindowRect.right	= width;
	WindowRect.top		= 0;
	WindowRect.bottom	= height;
	x = width;
	y = height;

	hInstance			= GetModuleHandle(NULL);
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc		= (WNDPROC) window::StaticProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= hInstance;
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= NULL;
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= "OpenGL";

	RegisterClass(&wc);
	
	dwExStyle	=	NULL;
	dwStyle		=	WS_POPUP		| WS_VISIBLE		|WS_SYSMENU;

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);

	hWnd = CreateWindowEx(	dwExStyle,
							"OpenGL",
							title,
							dwStyle,
							10, 10,
							WindowRect.right-WindowRect.left,
							WindowRect.bottom-WindowRect.top,
							NULL,
							NULL,
							hInstance,
							NULL);
	
	static	PIXELFORMATDESCRIPTOR pfd=	
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		bits,
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		16,	
		0,
		0,
		PFD_MAIN_PLANE,
		0,	
		0, 0, 0
	};
	
	hDC = GetDC(hWnd);

	PixelFormat = ChoosePixelFormat(hDC,&pfd);
	SetPixelFormat(hDC,PixelFormat,&pfd);


	hRC = wglCreateContext(hDC);

	wglMakeCurrent(hDC,hRC);
	
	ShowWindow(hWnd,SW_SHOW);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);	
	WindowResize(width, height);

	Init();

	return true;
}