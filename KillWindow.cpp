#include "window.h"
#include "link.h"

GLvoid window::KillWindow(GLvoid)
{
	if (hRC)
	{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(hRC);
		
		hRC=NULL;
	}

	ReleaseDC(hWnd,hDC);

	DestroyWindow(hWnd);

	UnregisterClass("OpenGL", hInstance);
}